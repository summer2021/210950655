import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import TagsInput from 'react-tagsinput'
import 'react-tagsinput/react-tagsinput.css' // If using WebPack and style-loader.
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

class Home extends React.Component {

	constructor() {
		super();
		this.state = {
			listeners: [],
			routes: [],
			tags: ["me", "you"
			]
		};
		URL = "http://183.84.2.169:8080"

	}
	componentDidMount() {
		// Runs after the first render() lifecycle
		var xhr = new XMLHttpRequest()
		xhr.open('GET', URL, true)
		xhr.onreadystatechange = x => {
			if (x.target.readyState == 4 && x.target.status == 200) {
				//import listeners
				const obj = JSON.parse(xhr.responseText)["config"]["listeners"]
				var listeners = []
				Object.keys(obj).forEach(function (key) {
					listeners.push({})
					listeners[listeners.length - 1][key] = obj[key]
				})
				this.setState({ listeners }, () => console.log(this.state.listeners));

				//import routes
				var routes = JSON.parse(xhr.responseText)["config"]["routes"]
				this.setState({ routes }, () => console.log(this.state.routes));
			}
		};
		xhr.send()
	}

	handleSubmit() {
		const json = {
			"listeners": {},
			"routes": []
		}
		//export listeners
		var temp = {}
		this.state.listeners.map((element) => {
			Object.keys(element).forEach(function (key) {
				temp[key] = element[key]
			})
		})
		json["listeners"] = temp
		//export routes
		json["routes"] = this.state.routes

		//then send AJAX to reconfig
		var xhr = new XMLHttpRequest()
		xhr.open('PUT', URL + "/config", true)
		xhr.onreadystatechange = function () {
			if (this.readyState == 4 && xhr.status == 200) {
				alert(xhr.responseText)
			}
		};
		console.log(JSON.stringify(json))
		xhr.send(JSON.stringify(json))
	}

	handleAddListener() {
		alert("AddListener")
		this.setState({
			listeners: [...this.state.listeners, {
				"": {
					"pass": ""
				}
			}]
		})
	}

	handleRemoveListener(index) {
		alert("Remove Listener " + index)
		const old = this.state.listeners;
		this.setState({
			listeners: [...old.slice(0, index), ...old.slice(index + 1)]
		});
	}

	handleChange = (tags) => {
		this.setState({ tags })
	}
	render() {

		return <div>
			<React.Fragment>
				<Typography variant="h4" gutterBottom>
					Listener
				</Typography>

				{this.state.listeners && this.state.listeners.map((element, index) => (
					<Grid container spacing={3}>
						<Grid item xs={12} sm={5}>
							<TextField required name="listenerIP" label="Host IP and Port" fullWidth
								defaultValue={Object.keys(element)}
								onChange={(e) => {
									var listener = this.state.listeners[index];
									var key = Object.keys(listener)[0] //ListenerIP
									var temp = listener[key]
									delete listener[key]
									listener[e.target.value] = temp
									this.setState({
										...this.state,
										listeners: [...this.state.listeners.slice(0, index), listener, ...this.state.listeners.slice(index + 1)]
									})
								}}
							/>
						</Grid>
						<Grid item xs={12} sm={5}>
							<TextField required name="listenerPass" label="Listener Pass" fullWidth
								defaultValue={element[Object.keys(element)]["pass"]}
								onChange={(e) => {
									var listener = this.state.listeners[index];
									var key = Object.keys(listener)[0]
									listener[key]["pass"] = e.target.value
									this.setState({
										...this.state,
										listeners: [...this.state.listeners.slice(0, index), listener, ...this.state.listeners.slice(index + 1)]
									})
								}}
							/>
						</Grid>
						<Grid item xs={12} sm={2}>
							<Button variant="outlined" color="secondary" onClick={() => this.handleRemoveListener(index)}>
								DELETE
							</Button>
						</Grid>
					</Grid>
				))}
				<br></br>
				<Button variant="outlined" onClick={() => this.handleAddListener()}>Add</Button>

				<br></br>

				<Typography variant="h4" gutterBottom>
					Route
				</Typography>

				{this.state.routes && this.state.routes.map((element, index) => (
					<Card variant="outlined">
						<CardContent>
							<Grid container spacing={1}>
								<Grid item xs={12}>
									<Typography variant="h5" gutterBottom> Route 1 Match</Typography>
								</Grid>

								<Grid item xs={8} >
									<TagsInput value={this.state.routes[0]["match"]["uri"]}
										onChange={(value) => {
											var routes = this.state.routes
											routes[index]["match"]["uri"] = value
											this.setState({ routes })
										}}
										inputProps={{ className: 'react-tagsinput-input', placeholder: 'Add a URI' }} />
								</Grid>

								<Grid item xs={5}>
									<TextField name="Host" label="Host" fullWidth
										defaultValue={this.state.routes[index]["match"]["host"]}
										onChange={(e) => {
											var routes = this.state.routes;
											routes[index]["match"]["host"] = e.target.value
											this.setState({ routes })
										}}
									/>
								</Grid>

								<Grid item xs={5}>
									<TextField name="Method" label="Method" fullWidth
										defaultValue={this.state.routes[index]["match"]["method"]}
										onChange={(e) => {
											var routes = this.state.routes;
											routes[index]["match"]["method"] = e.target.value
											this.setState({ routes })
										}}
									/>
								</Grid>


								<Grid item xs={12}>
									<Typography variant="h5" gutterBottom> Route 1 Action</Typography>
								</Grid>

								<Grid item xs={3}>
									<TextField name="share" label="Share" fullWidth
									/>
								</Grid>

								<Grid item xs={3}>
									<TextField name="pass" label="Pass" fullWidth
									/>
								</Grid>

								<Grid item xs={3}>
									<TextField name="proxy" label="Proxy" fullWidth
									/>
								</Grid>

								<Grid item xs={3}>
									<TextField name="return" label="Return" fullWidth
									/>
								</Grid>
							</Grid>
						</CardContent>
					</Card>
				))}



				<br></br>
				<Button variant="outlined" color="primary" onClick={() => this.handleSubmit()}>
					Submit
				</Button>

			</React.Fragment>
		</div>
	}
}

export default Home;

//TODO:
// 1. Route...
// 2. modal after submit
// 3. bugs when presenting add listener
